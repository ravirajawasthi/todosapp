import uuid from "uuid";

function todosReducer(todos, action) {
  switch (action.type) {
    case "addTodo":
      const newTodo = {
        id: uuid(),
        task: action.task,
        completed: false
      };
      return [...todos, newTodo];
    case "editTodo":
      const newTodos = todos.map(todo =>
        todo.id === action.id ? { ...todo, task: action.task } : todo
      );
      return newTodos;

    case "markComplete":
      return todos.map(todo =>
        todo.id === action.id ? { ...todo, completed: !todo.completed } : todo
      );
    case "removeTodo":
      return todos.filter(todo => todo.id !== action.id);

    default:
      return todos;
  }
}

export default todosReducer;
