import React, {useContext} from 'react';
import TextField from "@material-ui/core/TextField";
import useInputState from "./hooks/useInputState";
import {DispatchContext} from './contexts/todos.context'


export default function EditTodoForm({toggleIsEditing, id, task}){
    const dispatch = useContext(DispatchContext)  
    const [value, handleChange, reset] = useInputState(task);
    return(
        <form onSubmit={(e) => {
            e.preventDefault();
            dispatch({type: "editTodo", task: value, id: id})
            toggleIsEditing();
            reset();
        }} style={{margin: "1rem"}}>
            <TextField value={value} onChange={handleChange} autoFocus/>
        </form>
    )
}