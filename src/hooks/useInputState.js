import {useState} from 'react';

function useInputState(defaultValue){
    const [value, setstate] = useState(defaultValue);
    const setValue = (e) => {
        setstate(e.target.value);
    }
    const reset = () => {
        setstate("");
    }

    return [value, setValue, reset];
}

export default useInputState