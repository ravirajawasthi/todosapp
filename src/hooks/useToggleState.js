import {useState} from 'react';

function useToggle(initialVal = false){
    const [state, setstate] = useState(initialVal);

    const toggle = function(){
        setstate(!state)
    };
    return [state, toggle]
}

export default useToggle;