import { useReducer, useEffect } from "react";
import todosReducer from "../reducers/todos.reducer";

export default (key, initValue) => {
  const [todos, dispatch] = useReducer(
    todosReducer,
    JSON.parse(window.localStorage.getItem(key)) || initValue
  );

  // we use key and value as triggers because they are dependent on each other like a set
  useEffect(() => {
    window.localStorage.setItem(key, JSON.stringify(todos));
  }, [key, todos]);

  return [todos, dispatch];
};
