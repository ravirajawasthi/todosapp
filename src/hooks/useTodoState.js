import uuid from "uuid";
import useLocalStorangeHook from './useLocalStorageHook'
const defaultTodos = [{id: 1,task: "Add a task", completed: false }];
export default (initialTodos = defaultTodos) => {
  const [todos, setTodos] = useLocalStorangeHook("todos", initialTodos);
  return ({
    todos: todos,
    removeTodo: id => {
      const newTodos = todos.filter(todo => todo.id !== id);
      setTodos(newTodos);
    },

    markComplete: id => {
      const newTodos = todos.map(todo =>
        todo.id === id ? { ...todo, completed: !todo.completed } : todo
      );
      setTodos(newTodos);
    },
    editTodo: (id, value) => {
      const newTodos = todos.map(todo =>
        todo.id === id ? { ...todo, task: value } : todo
      );
      setTodos(newTodos);
    },
    addTodo: todoText => {
      const newTodo = {
        id: uuid(),
        task: todoText,
        completed: false
      };
      setTodos([...todos, newTodo]);
    }
  });
};
