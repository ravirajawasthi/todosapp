import React, { useContext, memo } from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import CheckBox from "@material-ui/core/Checkbox";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
import EditTodoForm from "./EditTodoForm";
import useToggleState from "./hooks/useToggleState";
import { DispatchContext } from "./contexts/todos.context";
import { ListItemSecondaryAction } from "@material-ui/core";

function Todo({ task, completed, id }) {
  const dispatch  = useContext(DispatchContext);
  const [isEditing, toggleIsEditing] = useToggleState(false);
  console.log(task, id)
  return (
    <>
      {isEditing ? (
        <EditTodoForm
          task={task}
          id={id}
          key={id}
          toggleIsEditing={toggleIsEditing}
        />
      ) : (
        <ListItem>
          <CheckBox
            onClick={() => dispatch({ type: "markComplete", id: id })}
            checked={completed}
          />
          <ListItemText style={{ textDecoration: completed && "line-through" }}>
            {task}
          </ListItemText>
          <ListItemSecondaryAction>
            <IconButton
              onClick={() => dispatch({ type: "removeTodo", id: id })}
            >
              <DeleteIcon />
            </IconButton>
            <IconButton onClick={toggleIsEditing}>
              <EditIcon />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      )}
    </>
  );
}
export default memo(Todo)