import React, { createContext } from "react";
import useLocalStorageReducerHook from '../hooks/useLocalStorageReducerHook'

const TodosContext = createContext();
const DispatchContext = createContext();
const defaultTodos = [{ id: 1, task: "Add a task!", completed: false }];
function TodosContextWrapper(props) {
  const [todos, dispatch] = useLocalStorageReducerHook("todos", defaultTodos);
  return (
    <TodosContext.Provider value={todos}>
      <DispatchContext.Provider value={dispatch}>
        {props.children}
      </DispatchContext.Provider>
    </TodosContext.Provider>
  );
}

export { TodosContextWrapper, TodosContext, DispatchContext };
