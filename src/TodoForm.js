import React, { useContext } from "react";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import useInputState from "./hooks/useInputState";
import { DispatchContext } from "./contexts/todos.context";

export default function TodoForm(props) {
  const [value, handleChange, reset] = useInputState("");
  const dispatch = useContext(DispatchContext);
  const handleaddTodo = function(e) {
    e.preventDefault();
    dispatch({ type: "addTodo", task: value });
    reset();
  };
  return (
    <Paper style={{ margin: "1rem 0", padding: "1rem 2rem" }}>
      <form onSubmit={handleaddTodo}>
        <TextField
          value={value}
          onChange={handleChange}
          fullWidth
          placeholder="Add new Todo"
        />
      </form>
    </Paper>
  );
}
