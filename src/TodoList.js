import React, { useContext } from "react";
import List from "@material-ui/core/List";
import Paper from "@material-ui/core/Paper";
import Todo from "./Todo";
import { TodosContext } from "./contexts/todos.context";
import { Divider } from "@material-ui/core";

export default function TodoList() {
  const todos = useContext(TodosContext);
  const n = todos.length - 1;
  if (todos.length > 0) {
    return (
      <Paper style={{ margin: "0", padding: "0" }}>
        <List>
          {todos.map((todo, i) => (
            <div key={todo.id}>
              <Todo
                task={todo.task}
                key={todo.id}
                id={todo.id}
                completed={todo.completed}
              />
              {i !== n && <Divider />}
            </div>
          ))}
        </List>
      </Paper>
    );
  }
  return null;
}
