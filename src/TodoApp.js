import React from "react";
import Paper from "@material-ui/core/Paper";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";
import Grid from "@material-ui/core/Grid";
import TodoList from "./TodoList";
import TodoForm from "./TodoForm";

export default function TodoApp() {
  return (
    <Paper
      style={{
        padding: "0",
        height: "100vh",
        margin: "0",
        backgroundColor: "#f5f5f5"
      }}
    >
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6">Todo App</Typography>
        </Toolbar>
      </AppBar>
      <Grid container justify="center">
        <Grid item xs={11} md={9} lg={5}>
          <TodoForm />
          <TodoList />
        </Grid>
      </Grid>
    </Paper>
  );
}
