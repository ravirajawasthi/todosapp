import React from "react";
import TodoApp from "./TodoApp";
import {TodosContextWrapper} from "./contexts/todos.context";
function App() {
  return (
    <TodosContextWrapper>
      <TodoApp />
    </TodosContextWrapper>
  );
}

export default App;
